import { provideRouter, RouterConfig }  from '@angular/router';

import { EventListComponent } from './event-list';
import { EventCreationComponent } from './event-creation';
import { EventDetailsComponent } from './event-details';

export const routes: RouterConfig = [
  { path: 'event-details/:id', component: EventDetailsComponent },
  { path: 'event-creation', component: EventCreationComponent },
  { path: '', component: EventListComponent }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];

