## 0. Just the generator code

We just added this file to keep track of the changes


## 1. Adding a component

We add a component using angular-cli:

> `$ ng g component event`

This creates an _event_ folder inside of `./app` containing a css, html, ts and spec.ts file alongside with a index.ts file which exports our module to the rest of the app.


## 2. Router

To implement the router we need to:

- Create a `routes.ts` file
- Import the routes file in our `main.ts` file & inject it into the app's bootstrap
- Import `ROUTER_DIRECTIVES` from the angular router into `app.component.ts`, our app's entry component
- Add the `router-outlet` directive to the `app.component.html` template


## 3. A host component

1. We add another component using:

> `$ ng g component eventList`

2. This component will make use of our `event` component

3. Then we update our `app.routes.ts` file to call `EventListComponent`

4. Now we import `EventComponent` into `EventListComponent`


## 4. A data service

1. We create a service using the cli:

> `$ ng g service dataService`

2. Now we import the data service from `eventListComponent` and inject it as a provider

3. We will be making http requests from the data service, so we need to include angular/http in our app's bootstrap

4. Once we have the data service code ready, we consume it from our `eventListComponent` and assign the response to bindable variable

5. Since the data is now available in `eventListComponent` we can display it in our template using the `{{}}` syntax


## 5. Displaying data

Angular includes several special components called **directives**, they are special because they can manipulate the DOM.

We also have pipes, a pipe lets us modify a value from inside its binding definition `{{aValue | aPipe}}`


## 6. Creating an event

1. First we create a component for the event creation concern
1. Then we add a route entry in `app.routes.ts` for the new component
1. Since we already have imported `ROUTER_DIRECTIVES` into `app.component` and injected it into our directives, we can safelly add a link to the new route in the template
1. We can create the _new event_ form now and give it some basic style
1. A POST handler is required in the data factory
1. Finally we use the _createEvent_ method of the data factory from `event-creation.component`

## 7. Reviewing a single event

1. Again we use `ng g component` to create a `eventDetails` component
1. Since this component will containt a _navigatable view_, it needs a route. We add a new entry to `app.routes.ts`
1. The data service now needs a method to request a single event based on its `id`
1. As before, we need to import the data service to use the new method we created
1. The router will provide us with the `id` of the event that needs to be fetched from the server, we need to get a hold of that `id` with the router's `ActivatedRoute` method
1. Once we have the `id` available we can make the call to our `getOneEvent` method
1. At this point we need to add a link to every event on the `event-list` component's template to open the `event-details` view. We'll need to import `ROUTER_DIRECTIVES`

## 8. Using the _event_ component

So far we haven't used the _event_ component we created at the beggining, its time to do it.
`eventComponent` will carry the whole 'single event' concern inside of it, the idea here is to reuse that component.
We need de same kind of logic in the main view and the `eventDetails`component's template.

1. We move all the `ROUTER_DIRECTIVES` import & directive injection out of `eventListComponent` into `eventComponent`
1. We also move the `isPastEvent` method
1. The CSS also needs to be moved since it is _component scoped_
1. The HTML template needs to be moved too, everyting except the ng-repeat
1. Now we replace that html with the `eventComponent`'s defined `selector`: `app-event`
1. We will pass the event data to every `eventComponent` instance using an _input_ called `data`
1. Since we are using an _input_ in the template, it needs to be imported from the component
1. The input values need to be specified in an annotation
1. The data we are expecting will come asynchronously so we need to listen to changes on it's binding. We use `OnChanges` & `SimpleChange`  
