import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
export class EventComponent implements OnInit {

  public event: {}

  @Input() data: any;
  @Input() isDetailed: boolean;

  constructor() {
    
  }

  ngOnInit() {
    if(this.isDetailed){
      this.event = this.event || {
        date: {
          start: 0,
          end: 0
        },
        name: "",
        description: "",
        speaker: {
          name: "",
          twitter: ""
        }
      };
    }
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if (changes["data"]) {
      console.log(changes["data"])
      this.event = changes["data"].currentValue;
    }
  }

  isPastEvent(eventDate) {
    if(!eventDate){
      return false;
    } 
    return eventDate.date.end < Date.now();
  }

}
