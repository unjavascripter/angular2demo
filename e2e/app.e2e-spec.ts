import { IWasTherePage } from './app.po';

describe('i-was-there App', function() {
  let page: IWasTherePage;

  beforeEach(() => {
    page = new IWasTherePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
