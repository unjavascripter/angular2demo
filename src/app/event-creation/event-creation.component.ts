import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';

@Component({
  moduleId: module.id,
  selector: 'app-event-creation',
  templateUrl: 'event-creation.component.html',
  styleUrls: ['event-creation.component.css'],
  providers: [DataServiceService]
})
export class EventCreationComponent implements OnInit {

  constructor(private dataS: DataServiceService) {}

  ngOnInit() {
  }

  createEvent(eventObj){
    this.dataS.createEvent(eventObj)
      .then(response => {
        console.log('response:');
        console.log(response)
      },(err => {
        console.warn(err);
      }));
  }

}
