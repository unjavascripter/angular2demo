import { Component, OnInit } from '@angular/core';
import { EventComponent } from '../event';
import { DataServiceService } from '../data-service.service';

@Component({
  moduleId: module.id,
  selector: 'app-event-list',
  templateUrl: 'event-list.component.html',
  styleUrls: ['event-list.component.css'],
  directives: [EventComponent],
  providers: [DataServiceService]
})
export class EventListComponent implements OnInit {
  
  public events = [];

  constructor(public dataSrvc: DataServiceService) {}

  ngOnInit() {
    this.dataSrvc.getEvents()
    .subscribe(event => {
      event.map(e => this.events.push(e));
    });
  }


}
