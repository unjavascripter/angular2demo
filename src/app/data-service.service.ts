import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise';


@Injectable()
export class DataServiceService {

  private apiURL = 'http://localhost:3000/api/things';

  constructor(private http: Http) {}

  getEvents(){
    return this.http
      .get(this.apiURL)
        .map(res => res.json());
        
  }

  getOneEvent(id){
    return this.http
      .get(`${this.apiURL}/${id}`)
        .toPromise()
          .then(res => res.json())
          .catch(err => err);
        
  }

  createEvent(eventObj) {
    return this.http
      .post(this.apiURL, eventObj)
        .toPromise()
          .then(res => res.json())
          .catch(err => err);

  }

}
