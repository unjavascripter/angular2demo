import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import { ActivatedRoute }       from '@angular/router';
import { EventComponent } from '../event';

@Component({
  moduleId: module.id,
  selector: 'app-event-details',
  templateUrl: 'event-details.component.html',
  styleUrls: ['event-details.component.css'],
  directives: [EventComponent],
  providers: [DataServiceService]
})
export class EventDetailsComponent implements OnInit {

  public event: {}

  constructor( private route: ActivatedRoute, private dataS: DataServiceService) {}

  ngOnInit() {
      this.route.params.subscribe(params => {
       this.dataS.getOneEvent(params["id"]).then(event => this.event = event, err => console.log(err));
     });
  }

}
